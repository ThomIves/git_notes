Basic Workflow: 
    # Do some programming.
    git status 
        # to see what files I changed.
    git diff [file] 
        # to see exactly what I modified.
    git commit -a -m [message] 
        # to commit.

Ways to refer to a specific commit:
    By its SHA1 name, which you can get from git log.
    By the first few characters of its SHA1 name.
    By a head. For example, HEAD refers to the commit 
        object referenced by HEAD. 
        You can also use the name, such as master.
    Relative to a commit. Putting a caret (^) after a commit name 
        retrieves the parent of that commit. 
        For example, HEAD^ is the parent of the current head commit.

Creating a Branch
    git branch [new-head-name] [reference-to-branch-to-build-from]

Switching Branches
    git checkout [head-name or branch-name]
    git checkout -b [new-head/branch-name]

Useful Branch Commands:
    git branch # lists existing heads, with * next to the current head.
    git diff [head1]..[head2] # shows diff between head2 and head1.
    git diff [head1]...[head2] # (three dots) 
        # Shows the diff between head2 and the common ancestor of 
        # head1 and head2. For example, diff master...fix-headers 
        # above would show the diff between (D) and (B).
    git log [head1]..[head2] # shows the change log between head2 
        # and the common ancestor of head1 and head2. 
        # With three dots, it also shows the changes between 
        # head1 and the common ancestor; this is not so useful. 
        # (Switching head1 and head2, on the other hand, is very useful.)

Merging
    git merge [head] # OK for when you are working solo 
        # Best to do from master 
        #      and head is latest branch name you want to merge to master
    git pull . [head] # This is better when collaborating
    git branch -d [head] # deletes a branch

Collaborating:
    git clone <some URL or location> 
        # copy remote project to local
    git remote set-url origin <new URL>
        # <new URL> example: "https://gitlab.com/ThomIves/tlc_pred.git"
        # to change the location of your remote "central" repo
        # in this example, I changed from my github-origin to gitlab
    git branch --track [new-local-branch] [remote-branch]
        # To work with an origin/feature remote branch locally, 
        # manually set up a tracking branch. 
    git fetch [remote-repository-reference] 
        # retrieves new commit objects in remote repository and 
        # creates and/or updates remote heads accordingly. 
        # By default, [remote-repository-reference] is origin.
    git fetch --prune
        # Forces the local repo to be just like the remote.
        # Branches may NOT show up after this with git status. 
        # Just check them out anyway, and they will be there.
    git pull [remote-repository-reference] [remote-head-name]
        # or just
    git pull
        # with no arguments will merge correct remote head
        # and will do a fetch, so you rarely need to do a fetch
    git push [remote-repository-reference] [remote-head-name]
        # or just
    git push
        # to push all branches in repo set up for tracking

Adding and Deleting Branches Remotely:
    To create a new branch on the remote repo, use the following, 
    while the new branch is your current head.
        git push --set-upstream origin new-branch
            # There are also options --track and --set-upstream 
            # for git branch to achieve similar effects.

    To delete a branch on the remote repository:
        git push [remote-repository-reference] :[head-name]
            # a colon before the head name means push “no data” 
            # into that head on the remote repository.

    To list the available remote branches, use ...
        git branch -r.

Rebasing:
    Need more time for this one to sink into my brain.
